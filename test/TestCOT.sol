pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/COT.sol";

contract TestCOT {

  function testInitialBalance() public {
    COT cot = COT(DeployedAddresses.COT());

    uint expected = 10000;

    Assert.equal(cot.balanceOf(tx.origin), expected, "Owner should have 10000 COT initially");
  }

  function testInitialBalanceWithNewCOT() public {
    COT cot = new COT();

    uint expected = 10000;

    Assert.equal(cot.balanceOf(this), expected, "Owner should have 10000 COT initially");
  }

}
