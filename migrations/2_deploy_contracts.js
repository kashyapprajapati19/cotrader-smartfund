var COT1 = artifacts.require("./COT.sol");
var SmartFund = artifacts.require("./SmartFund.sol");


module.exports = function(deployer) {
  deployer.deploy(COT1);
  deployer.deploy(SmartFund);
};
