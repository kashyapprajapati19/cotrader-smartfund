pragma solidity ^0.4.18;

import "./COT.sol";

contract SmartFund {
	using SafeMath for uint256;

    // Holds the ERC20 token COT
    COT m_tokenHandle;
    mapping (address => uint256) coTraderSmartBalances;

    struct KeyVal{
        string strKey;
        string strVal;
    }
    struct TraderFund{
        mapping (address => uint256) smartBalances; // CoTrader => COT
        string[] assetTypes;
        mapping (string => uint) portfolio;  // portfolio token => percentage
        uint portfolioTotal;
    }
    mapping (address => TraderFund) tradersInfo; // Trader => TraderFund.


    // struct AssetChangeReq{

    // }

    // Constructor
	function SmartFund(COT handleERCToken) public {
		m_tokenHandle = handleERCToken;
	}

    // Assume that the calling/message sender will be the CoTrader
	function add_to_fund(address addrTrader, uint256 fundAmount) public returns (bool){
        
        // Using the token handle add COT fund to contract address from CoTrader
        m_tokenHandle.transferFrom(msg.sender,this, fundAmount);
        coTraderSmartBalances[msg.sender].add(fundAmount);
        // Update the TraderFund with equal amount for the CoTrader
        tradersInfo[addrTrader].smartBalances[msg.sender].add(fundAmount);

        // Trigger the actual investments based on Trader portfolio distribution.
        return true;
    }

    // Assume that the calling/message sender will be the CoTrader
    function pull_from_fund(address addrTrader, uint256 fundAmount) public returns (bool){
        // using the token handle pull the COT fund back to  CoTrader Account from contract addresss
        require(tradersInfo[addrTrader].smartBalances[msg.sender] >= fundAmount);

        // Get the Required COT back selling according to portfolio.

        // Once done add the fund back to Trader COT from contract address
        m_tokenHandle.transferFrom(this, msg.sender, fundAmount);
        coTraderSmartBalances[msg.sender].sub(fundAmount);
        tradersInfo[addrTrader].smartBalances[msg.sender].sub(fundAmount);

        return true;
    }

    // Internal method - to check if particular Trader has Asset in their portfolio.
    function hasAssetType(address addrTrader, string strType) returns (bool){
        uint typeCount = tradersInfo[addrTrader].assetTypes.length;
        bool retVal = false;
        if( typeCount > 0){
            for(uint i=0; i<typeCount; ++i)
            {
                if(tradersInfo[addrTrader].assetTypes[i] == strType){
                    retVal = true;
                    break;
                }
            }
        }
        return retVal;
    }

    // 
    function updateTraderPortfolio(string strCurr, uint proportion) public {
        uint oldValue = tradersInfo[msg.sender].portfolio[strCurr];
        uint diff = proportion - oldValue;
        // Check for Valid updation - 
        if(tradersInfo[msg.sender].portfolioTotal + diff <= 100){
            
            // Update the new proportion value.
            tradersInfo[msg.sender].portfolio[strCurr] = proportion;
            tradersInfo[msg.sender].portfolioTotal.add(diff);
        }

        // TODO: Implement for change in investments.
    }


    // function change_smart_fund_portfolio(string assetA, )
	
}
